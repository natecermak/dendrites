#include <Rcpp.h>
using namespace Rcpp;


//' Linearly interpolate an arc to evenly-spaced points from a set of x-y coordinates
//' 
//' @param xy A two-column matrix, in which each row is a pair of x and y coordinates for a point.
//' @param nOut how many points should there be along the interpolated arc?
//' @export
// [[Rcpp::export]]
NumericVector interpArc(NumericMatrix xy, NumericVector nOut) {
  // name stolen from matlab function that does a similar thing.
  // no implied similarity in implementation or details (i didn't even look at at the matlab code)
  NumericMatrix out(nOut[0], 2);
  NumericVector cumDist(xy.nrow());
  cumDist[0] = 0;

  for (int i=1; i<xy.nrow(); i++)
    cumDist[i] = cumDist[i-1] + sqrt(  pow(xy(i,0)-xy(i-1,0), 2) +  pow(xy(i,1)-xy(i-1,1), 2) );

  double maxDist = cumDist(cumDist.size()-1);

  // first and last points are the same!
  out(0,0) = xy(0,0);
  out(0,1) = xy(0,1);
  out(out.nrow()-1,0) = xy(xy.nrow()-1,0);
  out(out.nrow()-1,1) = xy(xy.nrow()-1,1);

  int j = 1, i=0; // j indexes the interpolated data, i indexes original data
  double targetDist = maxDist/(nOut[0]-1) * j;
  double frac;

  // iterate over every interval in the original data (i indexes the original data)
  while( i < xy.nrow()-1 && j < out.nrow()-1){
    // if the target interpolated distance is in the middle between two points
    if (targetDist >= cumDist[i] && targetDist < cumDist[i+1]){
      frac = (targetDist - cumDist[i]) / ( cumDist[i+1]-cumDist[i]);
      out(j,0) = (1-frac)*xy(i,0) + frac*xy(i+1,0);
      out(j,1) = (1-frac)*xy(i,1) + frac*xy(i+1,1);
      j++;
      targetDist = maxDist/(nOut[0]-1) * j;
    } else{ // move to the next interval
      i++;
    }
  }

  return out;
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically
// run after the compilation.
//

/*** R
a = matrix(smooth(cumsum(rnorm(100)),twiceit=T),ncol=2);
b = interpArc(a,50);
c = cumsum(c(0, sqrt(diff(a[,1])^2  +diff(a[,2])^2)));

plot(a, type='o')
points(b, pch=16,cex=.5, col='red', type='o')
*/
