#include <Rcpp.h>
using namespace Rcpp;


//' Get edgelist from binary image
//' 
//' @param img A 2D matrix containing the image.
//' @return Returns a 2-column matrix containing indices of pixels that share an edge
//' @export
// [[Rcpp::export]]
IntegerMatrix getEdgeListFromBinaryImage(NumericMatrix img) {

  int brightPx = 0;
  for (int i =0; i<img.nrow();i++){
    for (int j=0; j<img.ncol(); j++){
      if(img(i,j)>0)
        brightPx++;
    }
  }
  
  IntegerMatrix edgeList(4*brightPx, 2);
  
  int p = 1;
  int e = 0;
  for (int j=0; j<img.ncol(); j++){
    for (int i=0; i<img.nrow();i++){ // select a pixel
      if (img(i,j) > 0){
        for(int di=-1; di<2; di++){  // now select a neighboring pixel, but only down and to the right
          if (i+di<0 || i+di >= img.nrow()) 
            continue;
          for(int dj=(di==1)?0:1; dj<2; dj++){
            if (j+dj<0 || j+dj >= img.ncol() || (di==0 && dj==0)) 
              continue;
            if (img(i+di,j+dj) > 0){  // if neighboring pixel is bright
              edgeList(e,0) = p;
              edgeList(e,1) = 1+(i+di)+(j+dj)*img.nrow();
              e++;
            }
          }
        }
      }
      p++;
    }
  }
  return(edgeList(Range(0,e-1),Range(0,1)));
}





// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//
/*** R

set.seed(1)
mat = matrix(rbinom(100,1,0.5),10,10);
image(1:10,1:10, mat)
text(rep(1:10,10),rep(1:10,each=10), 1:100)

library(dendRites)
E1 = getEdgeListFromBinaryImage(mat)
E2 = getEdgeListFromBinaryImage2(mat)

*/
