// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <Rcpp.h>

using namespace Rcpp;

// FHseg
IntegerVector FHseg(NumericVector edgeWeights, IntegerMatrix edgeList, int nNodes, double K);
RcppExport SEXP _dendRites_FHseg(SEXP edgeWeightsSEXP, SEXP edgeListSEXP, SEXP nNodesSEXP, SEXP KSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< NumericVector >::type edgeWeights(edgeWeightsSEXP);
    Rcpp::traits::input_parameter< IntegerMatrix >::type edgeList(edgeListSEXP);
    Rcpp::traits::input_parameter< int >::type nNodes(nNodesSEXP);
    Rcpp::traits::input_parameter< double >::type K(KSEXP);
    rcpp_result_gen = Rcpp::wrap(FHseg(edgeWeights, edgeList, nNodes, K));
    return rcpp_result_gen;
END_RCPP
}
// boxDilate
IntegerMatrix boxDilate(IntegerMatrix im, IntegerVector iters);
RcppExport SEXP _dendRites_boxDilate(SEXP imSEXP, SEXP itersSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< IntegerMatrix >::type im(imSEXP);
    Rcpp::traits::input_parameter< IntegerVector >::type iters(itersSEXP);
    rcpp_result_gen = Rcpp::wrap(boxDilate(im, iters));
    return rcpp_result_gen;
END_RCPP
}
// boxErode
IntegerMatrix boxErode(IntegerMatrix im, IntegerVector iters);
RcppExport SEXP _dendRites_boxErode(SEXP imSEXP, SEXP itersSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< IntegerMatrix >::type im(imSEXP);
    Rcpp::traits::input_parameter< IntegerVector >::type iters(itersSEXP);
    rcpp_result_gen = Rcpp::wrap(boxErode(im, iters));
    return rcpp_result_gen;
END_RCPP
}
// getEdgeListFromBinaryImage
IntegerMatrix getEdgeListFromBinaryImage(NumericMatrix img);
RcppExport SEXP _dendRites_getEdgeListFromBinaryImage(SEXP imgSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< NumericMatrix >::type img(imgSEXP);
    rcpp_result_gen = Rcpp::wrap(getEdgeListFromBinaryImage(img));
    return rcpp_result_gen;
END_RCPP
}
// hcSparse
List hcSparse(NumericMatrix dist, int method, NumericVector implicitDistance);
RcppExport SEXP _dendRites_hcSparse(SEXP distSEXP, SEXP methodSEXP, SEXP implicitDistanceSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< NumericMatrix >::type dist(distSEXP);
    Rcpp::traits::input_parameter< int >::type method(methodSEXP);
    Rcpp::traits::input_parameter< NumericVector >::type implicitDistance(implicitDistanceSEXP);
    rcpp_result_gen = Rcpp::wrap(hcSparse(dist, method, implicitDistance));
    return rcpp_result_gen;
END_RCPP
}
// interpArc
NumericVector interpArc(NumericMatrix xy, NumericVector nOut);
RcppExport SEXP _dendRites_interpArc(SEXP xySEXP, SEXP nOutSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< NumericMatrix >::type xy(xySEXP);
    Rcpp::traits::input_parameter< NumericVector >::type nOut(nOutSEXP);
    rcpp_result_gen = Rcpp::wrap(interpArc(xy, nOut));
    return rcpp_result_gen;
END_RCPP
}
// thinImage
IntegerMatrix thinImage(IntegerMatrix im, NumericVector maxIters);
RcppExport SEXP _dendRites_thinImage(SEXP imSEXP, SEXP maxItersSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< IntegerMatrix >::type im(imSEXP);
    Rcpp::traits::input_parameter< NumericVector >::type maxIters(maxItersSEXP);
    rcpp_result_gen = Rcpp::wrap(thinImage(im, maxIters));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_dendRites_FHseg", (DL_FUNC) &_dendRites_FHseg, 4},
    {"_dendRites_boxDilate", (DL_FUNC) &_dendRites_boxDilate, 2},
    {"_dendRites_boxErode", (DL_FUNC) &_dendRites_boxErode, 2},
    {"_dendRites_getEdgeListFromBinaryImage", (DL_FUNC) &_dendRites_getEdgeListFromBinaryImage, 1},
    {"_dendRites_hcSparse", (DL_FUNC) &_dendRites_hcSparse, 3},
    {"_dendRites_interpArc", (DL_FUNC) &_dendRites_interpArc, 2},
    {"_dendRites_thinImage", (DL_FUNC) &_dendRites_thinImage, 2},
    {NULL, NULL, 0}
};

RcppExport void R_init_dendRites(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
