
#include <Rcpp.h>
#include <cmath>

using namespace Rcpp;



int findHead(int i, IntegerVector PARENT){
  if (PARENT[i] == i)
    return(i);
  else {
    int p = findHead(PARENT[i], PARENT);
    PARENT[i] = p;
    return(p);
  }
}


// Perform Felzenszwalb-Huttenlocher segmentation
// 
// @param edgeWeights a numeric vector containing edge weights. must be sorted in increasing order!
// @param edgeList a 2-column matrix where each row containing indices of elemnts that share a edge (in the same order as edgeWeights)!
// @param nNodes number of nodes in the graph 
// @param K tuning parameter for size of components. smaller numbers = more components.
// [[Rcpp::export]]
IntegerVector FHseg(NumericVector edgeWeights, IntegerMatrix edgeList, int nNodes, double K) {

  IntegerVector PARENT(nNodes);
  IntegerVector RANK(nNodes);
  IntegerVector SIZE(nNodes);
  NumericVector INTDIS(nNodes);
  
  for (int i = 0; i < nNodes; i++){
    PARENT(i) = i;
    SIZE(i) = 1;
    RANK(i) = 0;
    INTDIS(i) = 0;
  }
  
  int m1, m2, irep, jrep, newHead, sub;
  double newIntDis;
  
  for (int i=0; i<edgeList.nrow(); i++ ){         // 3. for each edge in the original g
    m1 = findHead(edgeList(i,0), PARENT);
    m2 = findHead(edgeList(i,1), PARENT);
    if (m1 != m2){
      if (edgeWeights[i] < std::min( INTDIS[m1] + K/SIZE[m1], INTDIS[m2] + K/SIZE[m2])) { //if edge weight is small compared to MInt(C1, C2)))
        newIntDis = std::max(INTDIS[m1], INTDIS[m2]);
        newIntDis = std::max(newIntDis, edgeWeights[i]);
        
        // DO THE MERGE:
        irep = findHead(m1, PARENT); 
        jrep = findHead(m2, PARENT);
        if (RANK[irep] < RANK[jrep]) {
          newHead = jrep; 
          sub = irep;
        } 
        if (RANK[irep] >= RANK[jrep]){
          newHead = irep;
          sub=jrep;
        }
        PARENT[sub] = newHead;                      // merge trees
        INTDIS[newHead] = newIntDis;                // update internal dissimilarity of head node
        SIZE[newHead] += SIZE[sub];                 // update SIZE
        if  (RANK[irep] == RANK[jrep])              // update RANK
          RANK[newHead]++; 
        // END MERGE
      }
    }
  }

  IntegerVector membership(nNodes);
  for (int i = 0; i < nNodes; i++)
    membership[i] = findHead(i, PARENT);
  return(membership);
  
}





// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R

*/

