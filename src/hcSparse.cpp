#include <Rcpp.h>
#include <cmath>
using namespace Rcpp;


//' Hierarchical clustering with a sparse distance matrix (provided in triplet form)
//' 
//' @param dist A 3-column matrix where the first column is the row index, second is the column index, and 
//' third is the distance between those two.
//' @param method An integer. 1 implies single linkage (nearest neighbor) whereas 2 implies complete linkage
//' (furthest neighbor).
//' @param implicitDistance A number that specifies the distance between points not included in the sparse
//' distance matrix.
//' @export
// [[Rcpp::export]]
List hcSparse(NumericMatrix dist, int method, NumericVector implicitDistance) {
  //assumes dist is upper triangular, sparse (in triplet format), without diagonal elements

  int to, from, toGroup, fromGroup, newGroup;

  int N=0;                        // first, determine total number of things we need to cluster
  for (int i=0; i < dist.nrow(); i++){
    if (dist(i,0) > N) N = dist(i,0);
    if (dist(i,1) > N) N = dist(i,1);
    dist(i,0) = dist(i,0)-1;     // R indexes from 1, so subtract one from each index for C indexing
    dist(i,1) = dist(i,1)-1;
  }

  NumericMatrix m(N-1, 3);    // tracks merges, first 2 cols are group indices, third is height
  IntegerVector groupSize(N+1,0); // tracks size of each group for groups with >= 2 members
  IntegerVector n(N);         // tracks group membership
  for (int i=0; i<N; i++)     // initialize as all negative (positive are groups)
    n[i] = -(i+1);

  NumericVector A(N, implicitDistance[0]);         // for non-sparse distance VECTOR
  NumericVector B(N, implicitDistance[0]);         // for non-sparse distance VECTOR
  NumericVector R(N, implicitDistance[0]);         // for non-sparse distance VECTOR

  // prepare for loop by getting link with minimal distance
  double minDist = dist(0,2); // col2 is distance
  int minRow = 0;
  for (int i=0; i < dist.nrow(); i++){
    if (dist(i,2) < minDist){
      minDist = dist(i,2);
      minRow = i;
    }
  }

  int iter=0;

  while (iter < N-1 && minDist < R_PosInf){
    if (iter % 100 == 0) printf("N=%d, iter %d, minDist=%.03f\n",N, iter, minDist);

    to = (int) dist(minRow,0);
    from = (int) dist(minRow,1);
    toGroup = n[to];
    fromGroup = n[from];

    m(iter,0) = (toGroup<fromGroup)?toGroup:fromGroup;       // keep track of the two groups we're merging together
    m(iter,1) = (toGroup<fromGroup)?fromGroup:toGroup;       // (same here)
    m(iter,2) = dist(minRow,2);  // keep track of the height at which we're merging

    // update group memberships (we'll call the new group # iter)
    newGroup = iter+1;
    n[to] = newGroup;
    n[from] = newGroup;
    // update group sizes
    for (int i=0; i<=N; i++)
      if (n[i] == toGroup || n[i] == fromGroup) groupSize[newGroup]++;
    if (toGroup>0) groupSize[toGroup]=0;
    if (fromGroup>0) groupSize[fromGroup]=0;
    // update group memberships in distance matrix
    for (int i=0; i<dist.nrow(); i++){
      if (n[dist(i,0)] == toGroup || n[dist(i,0)] == fromGroup) n[dist(i,0)] = newGroup;
      if (n[dist(i,1)] == toGroup || n[dist(i,1)] == fromGroup) n[dist(i,1)] = newGroup;
    }

    /**** update distances ****/
    // first calculate each non-sparse distance vector for the two groups to be merged.
    std::fill(A.begin(), A.end(), implicitDistance[0]);
    std::fill(B.begin(), B.end(), implicitDistance[0]);
    std::fill(R.begin(), R.end(), implicitDistance[0]);

    for (int i=0; i < dist.nrow(); i++){
      if (dist(i,0) == from)  A[ (int) dist(i,1) ] = dist(i,2);
      else if (dist(i,0) == to)    B[ (int) dist(i,1) ] = dist(i,2);
      if (dist(i,1) == from)  A[ (int) dist(i,0) ] = dist(i,2);
      else if (dist(i,1) == to)    B[ (int) dist(i,0) ] = dist(i,2);
    }
    // second, calculate the pmin/pmax of the two vectors
    for (int i=0; i < N; i++){
      if (method==1) R[i] = (A[i]<B[i])?A[i]:B[i];  // single-linkage - take the smallest distance
      else if (method==2) R[i] = (A[i]<B[i])?B[i]:A[i];  // complete linkage - take the largest distance
    }
    R[to] = R_PosInf; // new cluster cannot connect to itself (infinite distance to its own internal nodes)
    R[from] = R_PosInf;

    // third update the distance matrix
    for (int i=0; i < dist.nrow(); i++){
      //if this distance involves from or to
      if (dist(i,0)==from && dist(i,1)!=to  )  dist(i,2) = R[(int) dist(i,1)];
      else if (dist(i,0)!=to   && dist(i,1)==from)  dist(i,2) = R[(int) dist(i,0)];
      else if (dist(i,0)==to   && dist(i,1)!=from) {dist(i,2) = R[(int) dist(i,1)]; dist(i,0)=from; }
      else if (dist(i,0)!=from && dist(i,1)==to  ) {dist(i,2) = R[(int) dist(i,0)]; dist(i,1)=from; }
      else if ((dist(i,0)==from && dist(i,1)==to) || (dist(i,0)==to && dist(i,1)==from)) dist(i,2) = R_PosInf;
    }

    // find the newest minimum distances
    minDist = R_PosInf;
    minRow = 0;
    for (int i=0; i < dist.nrow(); i++){
      if (dist(i,2) < minDist && n[dist(i,0)] != n[dist(i,1)]){
        minDist = dist(i,2);
        minRow = i;
      }
    }
    iter++;
  }
  if (iter < N-1)
    printf("Reached maximal clusterning of distance matrix. Now beginning merging trees at single height.\n");

  // we've done all the clustering we can. now if we're not fully merged, merge groups randomly so that we have a single tree.
  fromGroup = n[0];
  for(; iter < N-1; iter++){
    if (iter % 100 == 0) printf("iter %d\n",iter);

    for (int i = 0; i < N; i++){ // find the group to merge it with
      if (n[i]!=fromGroup) {
        toGroup = n[i];
        break;
      }
    }
    m(iter,0) = fromGroup;       // update the merge list
    m(iter,1) = toGroup;
    m(iter,2) = implicitDistance[0];
    for (int i=0; i < N; i++){ // update the grouping vector
      if (n[i]==toGroup || n[i] == fromGroup) {
        n[i] = iter+1;
      }
    }
    fromGroup = iter+1;
  }

  NumericMatrix merge = m(_,  Range(0,1));
  NumericVector height = m(_, 2);

  printf("Determining ordering.\n");
  IntegerVector order(N+1,0);      // order uses R indices
  order[1] = m(N-2,0);
  order[2] = m(N-2,1);

  int loc=2, loc2;
  for(int i = N-2; i >= 1; i--)  { // i is in R indices (indexes from 1)
    loc2 = loc;
    for(int j=1; j <= loc2; j++)  { // j is in R indices
      if(order[j] == i) {
        order[j] = m(i-1,0);
        if(j==loc) {
          loc++;
          order[loc] = m(i-1,1);
        } else {
          loc++;
          if (loc <= j+2)
            for(int k=loc; k <= j+2; k++)
              order[k] = order[k-1];
          else if (loc > j+2)
            for(int k=loc; k >= j+2; k--)
              order[k] = order[k-1];
          order[j+1] = m(i-1,1);
        }
      }
    }
  }
  for (int i=0; i<=N; i++)
    order[i] = -order[i];

  List retVal;
  retVal["merge"] = merge;
  retVal["height"] = height;
  retVal["order"] = order[Range(1,N)];
  retVal.attr("class") = "hclust";
  return(retVal);
}

// FOR TESTING
/*** R


set.seed(1);
library(Matrix)
nNonzero=4e4
nElements = 1e4
rows = sample(1:nElements, nNonzero, replace=TRUE)
cols = sample(1:nElements, nNonzero, replace=TRUE)
j = which(rows>cols)
tmp = rows[j]
rows[j] = cols[j]
cols[j] = tmp
a = runif(nNonzero)
d = cbind(rows, cols, a)

system.time({
  h = hcSparse(d, method=1, implicitDistance=10)
})
plot(h, labels=rep('', length(h$order)))

# now try it via hclust
m = matrix(10,nrow=nElements, ncol=nElements)
m[cbind(rows, cols)] = a;
m[cbind(cols, rows)] = a;
distObj = as.dist(m)
system.time({ h1 = hclust(distObj, method='single') })
plot(h1, labels=rep('', nrow(m)))

*/

